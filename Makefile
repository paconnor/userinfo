ROOTINC    = $(shell root-config --incdir)
ROOTCFLAGS = $(shell root-config --cflags)
ROOTLIBS   = $(shell root-config --libs)

BOOSTINC ?= /usr/include/boost

CFLAGS=-g -Wall -O3 -std=c++1z -fPIC -DDEBUG -lrt -pthread -lresolv

all: libUserInfo.so test 

lib%.so: %.cc %.h
	g++ $(CFLAGS) $(ROOTCFLAGS) -shared $< -I$(BOOSTINC) -I$(ROOTINC) $(ROOTLIBS) -o $@ 

test: test.cc UserInfo.h
	g++ $(CFLAGS) $(ROOTCFLAGS) $< -I$(ROOTINC) -o $@ $(ROOTLIBS) -L. -Wl,-rpath,. -lUserInfo -lrt

clean: 
	rm -f *.pdf lib*.so *.o test

.PHONY: all clean

