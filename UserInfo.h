#ifndef USERINFO_H
#define USERINFO_H

#include <cassert>

#include <type_traits>
#include <iostream>
#include <filesystem>
#include <typeinfo>

#include <TList.h>
#include <TString.h>
#include <TTree.h>
#include <TParameter.h>
#include <TObjString.h>

#include <boost/property_tree/ptree.hpp>      

[[ maybe_unused ]]
static const char * green = "\x1B[32m",
                  * red   = "\x1B[31m",
                  * bold  = "\e[1m",
                  * def   = "\e[0m";

////////////////////////////////////////////////////////////////////////////////
/// Generic class to read the meta-information contained in a list.
/// The original goal is to read and write information to store in the list
/// `TTree::GetUserInfo()`, but it can be used in general.
///
/// The elements of the list may simply be reached with `UserInfo::Get()`
/// and written with `UserInfo::Set()`. The list may contain simple types
/// such as integral types or literals as well as other lists.
/// To reach a list, the `Get` and `Set` function simply need the list of 
/// keys to navigate through the tree structure, provided with additional 
/// arguments.
///
/// The whole information may be extracted in json, info, or xml format.
///
/// This class is as generic as can be. In certain cases, one may want to 
/// use inherit from this class to enforce a more 
///
/// No sophisticated exceptions are foreseen. 
class UserInfo {

    const bool own; //!< boolean to know if the list should be deleted in the destructor
    TList * l; //!< main list (typically from `TTree::GetUserInfo()`)

protected:

    ////////////////////////////////////////////////////////////////////////////////
    /// Accest do daughter list from a mother list
    TList * List (TList * mother, const char * key) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Final getter. The behaviour for integral types and for literal (default) 
    /// is slightly different, since literals don't really have a key.
    template<class T> T Get (TList * mother, const char * key) const
    {
        TObject * obj = mother->FindObject(key);
        if (obj == nullptr) {
            std::cerr << red << "The object `" << key << "` of type `" << typeid(T).name() << "` has not been found!\n" << def;
            exit(EXIT_FAILURE);
        }

        if constexpr (std::is_arithmetic_v<T>) {
            auto parameter = dynamic_cast<TParameter<T>*>(obj);
            assert(parameter);
            return parameter->GetVal();
        }
        else {
            auto objs = dynamic_cast<TObjString*>(obj);
            assert(objs);
            T literal = objs->GetString().Data();
            return literal;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Recursive getter
    template<class T, typename... Args> T Get (TList * mother, const char * key, Args... args) const
    {
        TList * daughter = List(mother, key);
        return Get<T>(daughter, args...);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Recursive finder
    template<typename... Args> bool Find (TList * mother, const char * key)
    {
        TObject * obj = mother->FindObject(key);
        return (obj != nullptr);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Recursive finder
    template<typename... Args> bool Find (TList * mother, const char * key, Args... args)
    {
        TList * daughter = List(mother, key);
        return Find(daughter, key, args...);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// (Re)sets elements of a given list.
    ///
    /// Only integral types have a key and a value.
    /// Literals only have identical key and value.
    template<class T> void Set (TList * mother, const char * key, T value) const
    {
        if constexpr (std::is_arithmetic_v<T>) {
            TObject * obj = mother->FindObject(key);
            if (obj) {
                //const TString classname = Form("TParameter<%s>", typeid(T)); // typeid does not work as I want with g++... for `int`, it will only return `i`, which makes the check cumbersome...
                //assert(expected == obj->ClassName()); // TODO: rather throw an exception?
                auto parameter = dynamic_cast<TParameter<T>*>(obj);
                parameter->SetVal(value);
            }
            else {
                auto parameter = new TParameter<T>(key, value, 'l');
                mother->Add(parameter);
            }
        }
        else {
            TList * daughter = List(mother, key);
            TString str(value);
            auto objs = new TObjString(str);
            daughter->Add(objs);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Recursive setter
    template<class T, typename... Args>
    void Set (TList * mother, const char * key, Args... args) const
    {
        TList * daughter = List(mother, key);
        Set<T>(daughter, args...);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Recursive call of `TList::Print()`
    inline void Print (TList *) const;

public:
    ////////////////////////////////////////////////////////////////////////////////
    /// Access directly a sublist
    template<typename... Args> TList * List (const char * key, Args... args)
    {
        return List(l, key, args...);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Access directly to main list
    inline TList * List ()
    {
        return l;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Generic getter
    template<class T, typename... Args> T Get (const char * key, Args... args)
    {
        return Get<T>(l, key, args...);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Generic finder
    template<typename... Args> bool Find (const char * key, Args... args)
    {
        return Find(l, key, args...);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Generic setter. The key is the name of the first sublist (if any), or to 
    /// the element directly. An extra argument (implicitly in the variadic argument)
    /// contains the value.
    template<class T, typename... Args> void Set (const char * key, Args... args)
    {
        Set<T>(l, key, args...);
    }

    UserInfo (const char * = "UserInfo"); //!< Constructor (starting from scratch)
    UserInfo (TList *);  //!< Constructor (starting from existing list)
    ~UserInfo (); //!< Destructor

    void Print () const; //!< Calls `UserInfo::Print()` with the main list
    boost::property_tree::ptree Write (const std::filesystem::path&) const; //!< Writes to file with Boost
};

#endif 
