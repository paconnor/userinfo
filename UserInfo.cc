#include "UserInfo.h"

#include <boost/preprocessor/seq/elem.hpp>
#include <boost/preprocessor/seq/for_each.hpp>

#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <functional>
#include <utility>

#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>
namespace pt = boost::property_tree;

using namespace std;
namespace fs = filesystem;

TList * UserInfo::List (TList * mother, const char * key) const
{
    TObject * obj = mother->FindObject(key);
    if (obj == nullptr) {
        auto daughter = static_cast<TList *>(obj);
        daughter = new TList;
        daughter->SetName(key);
        mother->Add(daughter);
        return daughter;
    }
    else {
        TString classname = obj->ClassName();
        if (classname != "TList") {
            cerr << red << "The object exists but is not a `TList`: " << classname << '\n' << def;
            exit(EXIT_FAILURE);
        }
        auto daughter = dynamic_cast<TList *>(obj);
        return daughter;
    }
}

void UserInfo::Print (TList * list) const
{
    list->Print();
    for (const auto& obj: *list) {
        TString classname = obj->ClassName();
        if (classname == "TList")
            Print(dynamic_cast<TList*>(obj));
    }
}

void UserInfo::Print () const
{
    Print(l);
}

enum Type { INFO, JSON, XML };

pt::ptree UserInfo::Write (const fs::path& p) const
{
    Type type = INFO;
    if (p.extension() == ".json")
        type = JSON;
    else if (p.extension() == ".xml")
        type = XML;

    function<pt::ptree(TList * list)> loop;
    loop = [p,&loop,&type](TList * list) {
        assert(list);
        pt::ptree tree; // this tree will contain the list
        for (const auto& obj: *list) {
            const char * key = obj->GetName();
            const TString classname = obj->ClassName();
            if (classname == "TList") {
                auto sublist = dynamic_cast<TList *>(obj);
                pt::ptree child = loop(sublist);
                tree.add_child(key, child);
            }
            else if (classname == "TObjString") {
                auto objs = dynamic_cast<TObjString *>(obj);
                const char * value = objs->GetString().Data();

                switch (type) {
                    case INFO: tree.add<const char *>(value, "\0");
                               break;
                    case XML: tree.add("string", value);
                              break;
                    case JSON:
                        {
                               pt::ptree node;
                               node.put("", value);
                               tree.push_back(make_pair("", node));
                        }
                }
            }
#define ARITHMETIC_TYPES (bool)(char)(char16_t)(char32_t)(wchar_t)(signed char)(short int)(int)(long int)(long long int)(unsigned char)(unsigned short int)(unsigned int)(unsigned long int)(unsigned long long int)(float)(double)(long double)
#define ELSE_IF_TPARAM(r, data, TYPE) \
            else if (classname == "TParameter<" BOOST_PP_STRINGIZE(TYPE) ">") { \
                auto param = dynamic_cast<TParameter<TYPE>*>(obj); \
                tree.add<TYPE>(key, param->GetVal()); \
            }
            BOOST_PP_SEQ_FOR_EACH(ELSE_IF_TPARAM, _, ARITHMETIC_TYPES)
#undef ARITHMETIC_TYPES
#undef ELSE_IF_TPARAM
            else {
                cerr << red << "Warning: " << classname << " is not handled by " << __func__ << ", therefore `" << key << "` will not be written to the `" << p << "`.\n" << def;
                //exit(EXIT_FAILURE); // TODO?
            }
        }
        return tree;
    };
    pt::ptree tree = loop(l);

    switch (type) {
        case JSON:
            write_json(p.c_str(), tree);
            break;
        case XML :
        {
            pt::xml_writer_settings<string> settings(' ', 2);
            write_xml (p.c_str(), tree, std::locale(), settings);
            break;
        }
        case INFO:
        {
            stringstream ss;
            write_info(ss, tree);
            string str = ss.str();
            size_t pos = str.find("\"\"");
            while (pos != string::npos) {
                str.replace(pos,2,"");
                pos = str.find("\"\"");
            }
            ofstream file(p.c_str());
            file << str;
            file.close();
        }
    }

    return tree;
}

UserInfo::UserInfo (const char * name) :
    own(true), l(new TList)
{
    l->SetName(name);
}

UserInfo::UserInfo (TList * L) :
    own(false), l(L)
{
    Print(l);
}

UserInfo::~UserInfo ()
{
    Print(l);
    if (own)
        delete l;
}
