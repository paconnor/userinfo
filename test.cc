#include "UserInfo.h"

using namespace std;

int main (int argc, char * argv[])
{
    UserInfo userinfo;

    userinfo.Set<bool>("isMC", true);
    userinfo.Set<int>("year", 2016);
    userinfo.Set<bool>("HIPM", true);
    userinfo.Set<int>("CMSSW", 1234);
    userinfo.Set<const char *>("corrections", "METfilters");
    userinfo.Set<const char *>("corrections", "hotzones");
    userinfo.Set<const char *>("corrections", "HEMzones");
    userinfo.Set<const char *>("corrections", "JES");
    userinfo.Set<const char *>("corrections", "trigger");
    userinfo.Set<const char *>("corrections", "pileup");
    userinfo.Set<const char *>("variations", "JES", "nominal");
    userinfo.Set<const char *>("variations", "JES", "JESup");
    userinfo.Set<const char *>("variations", "JES", "JESdn");
    userinfo.Set<const char *>("variations", "evJetWgt", "nominal");
    userinfo.Set<const char *>("variations", "evJetWgt", "prefup");
    userinfo.Set<const char *>("variations", "evJetWgt", "prefdn");

    cout << userinfo.Get<bool>("isMC") << '\n'
         << userinfo.Get<int>("year") << '\n'
         << userinfo.Get<bool>("HIPM") << '\n'
         << userinfo.Get<int>("CMSSW") << endl;

    userinfo.Write("out.info");
    userinfo.Write("out.json");
    userinfo.Write("out.xml");

    return EXIT_SUCCESS;
}
